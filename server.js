var express = require("express");
var jsonParser = require("body-parser").json();
var rawParser = require("body-parser").raw({ type: "application/jwt" });
var CryptoJS = require("crypto-js");
var mysql = require("mysql");
//var sendMail = require("./mail");
var app = express();
var port = process.env.PORT || 8081;
const nodemailer = require('nodemailer');
const bcrypt = require('bcrypt');
var cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
// var LocalStorage  =  require('node-localstorage');
// localStorage = new LocalStorage('./scratch');
//var flash = require('connect-flash');

// var { LocalStorage } = require('node-localstorage')
// localStorage = new LocalStorage('./scratch')
require('dotenv').config()


app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// This is the user "database", replace with something real
var secrets = {
	"user@example.com": {
		"secret": "abc123",
		"salt": ""
	}
};

var currentUser = "";

//app.use('/templates/dashboard.html', express.static(__dirname + '/templates/images'));
app.use((req, res, next) => {
	console.log("Request for: ", req.url);
	next();
});
app.use(express.static('www'));
app.use(express.static('templates'));
app.use(express.static('/templates/dash'));
//app.use(flash);
//app.use(express.json);
app.use(express.urlencoded({
	extended: false
}));
app.use(rawParser);
app.use(jsonParser);
app.use((req, res, next) => {
	console.log("resetting user");
	currentUser = "";
	next();
});
app.use(cookieParser());

var transporter = nodemailer.createTransport({
	service: 'gmail',
	auth: {
		user: 'neel2570@gmail.com',
		pass: process.env.PASSWORD
	}
});
// SQL Database Connection 
var con = mysql.createConnection({
	host: "localhost",
	user: "root",
	password: "password",
	database: "emailverify",
	//insecureAuth: true
});

var con2 = mysql.createConnection({
	host: "localhost",
	user: "root",
	password: "password",
	database: "userinfo",
	//insecureAuth: true
});

con.connect(function (err) {
	if (err) throw err;
	console.log("Connected!");
});
con2.connect(function (err) {
	if (err) throw err;
	console.log("Connected2!");
});
app.get("/product", function (req, res) {
	var fs = require("fs");
	var file = fs.readFileSync(__dirname + "/templates/dash/product.html");
	res.status(200);
	res.end(file);
});

app.get("/blogs", function (req, res) {
	var fs = require("fs");
	var file = fs.readFileSync(__dirname + "/templates/dash/blogs.html");
	res.status(200);
	res.end(file);
});

app.get("/admin", function (req, res) {
	res.sendFile(__dirname + '/templates/dash/admin.html');
	// var role1 = JSON.parse(localStorage.getItem("userinfo")).role;

	// if (role1 == 'admin') {
	// 	res.sendFile(__dirname + '/templates/dash/admin.html');
	// }
	// else {
	// 	res.send("You don't have access to this page");
	// }

	// con.query("select * from user_manage", function (err, result) {
	// 	if (err) throw err;
	// 	res.end(JSON.stringify(result)
	// });

});
app.get("/getdata", function (req, res) {
	con.query("select * from user_manage", function (err, result) {
		if (err) throw err;
		res.end(JSON.stringify(result));
	});
});

app.get("/profile", function (req, res) {
	var fs = require("fs");
	var file = fs.readFileSync(__dirname + "/templates/dash/profile.html");
	res.status(200);
	res.end(file);
});
app.post("/profile", function (req, res) {
	var newp = req.body.newp;
	var currentp = req.body.currentp;
	var email = req.body.Email;

	bcrypt.genSalt(10, function (err, salt) {
		bcrypt.hash(newp, salt, function (err, hash) {
			if (err) {
				console.log(err);
			} else {
				Change(hash);
			}
		});
	});
	function Change(newPassHash) {

		con.query("UPDATE verify SET password = \'" + newPassHash + "\' where email = ?", email, (err, result) => {
			if (err) {
				console.log(err);
			}
			else {

			}
		});

	}
});

app.get("/users", function (req, res) {
	var fs = require("fs");
	var file = fs.readFileSync(__dirname + "/templates/dash/users.html");
	res.status(200);
	res.end(file);
});
app.post("/users", function (req, res) {
	var Uname = req.body.Username;
	var name = req.body.Name;
	var address = req.body.Address;
	var tel = req.body.Tel;
	var Uemail = req.body.Uemail;

	var uData = { Username: Uname, Name: name, Address: address, PhoneNo: tel, Email: Uemail };
	con.query("INSERT INTO user_manage SET ?", uData, (err, result) => {
		if (err) {
			console.log(err)
		}
		else {
			console.log("Data Inserted...");
			//req.flash("success","Data Inserted..");
		}
	});
})

app.get("/contact", function (req, res) {
	var fs = require("fs");
	var file = fs.readFileSync(__dirname + "/templates/dash/contact.html");
	res.status(200);
	res.end(file);
});

app.get("/map", function (req, res) {
	var fs = require("fs");
	var file = fs.readFileSync(__dirname + "/templates/dash/map.html");
	res.status(200);
	res.end(file);
});


// COUNT query execution
app.get("/getdata1", function (req, res) {

	con2.query("select count(gender) from user where gender='male'", function (err, result) {
		if (err) throw err;
		res.end(JSON.stringify(result));
	});
});
app.get("/getdata2", function (req, res) {

	con2.query("select count(gender) from user where gender='female'", function (err, result) {
		if (err) throw err;
		//console.log(result);
		//res.write("No. of Female: ");
		res.end(JSON.stringify(result));
	});
});
app.get("/getdata3", function (req, res) {

	con2.query("select count(catagory) from user where catagory='business'", function (err, result) {
		if (err) throw err;
		//console.log(result);
		//res.write("No. of people doing business: ");
		res.end(JSON.stringify(result));
	});
});
app.get("/getdata4", function (req, res) {

	con2.query("select count(catagory) from user where catagory='job'", function (err, result) {
		if (err) throw err;
		//console.log(result);
		//res.write("No. of people doing job: ");
		res.end(JSON.stringify(result));
	});
});



// app.get("/", function (req, res) {
// 	console.log("index non-authd");
// 	res.sendFile(__dirname + "/templates/index.html");
// });

// app.get("/images", function(req,res){
// 	res.sendFile(__dirname + "/templates/images/tt.jpg");
// });


app.post('/', (req, res) => {

	// verification
	function Store(pass) {
		var uname = req.body.Username;
		var verify = Math.floor((Math.random() * 10000000) + 1);

		var mailOption = {
			from: 'neel2570@gmail.com', // sender this is your email here
			to: `${req.body.Email}`, // receiver email2
			subject: "Account Verification",
			html: `<a href="http://localhost:8081/verification/?verify=${verify}">CLICK ME TO ACTIVATE YOUR ACCOUNT</a>`
		}
		// store data 


		con.query('SELECT * FROM verify WHERE email = ?', req.body.Email, (err, result) => {
			if (err) {
				console.log(err);
			} else {


				if (result[0] != null) {

					console.log("Email is already registered and verification link is sent.")

					res.json({ emailAlready: "emailAlready" });


					transporter.sendMail(mailOption, (error, info) => {
						if (error) {
							console.log(error)
						}
						else {
							let userdata = {
								email: `${req.body.Email}`,
							}

							// res.cookie("userinfo", userdata);
							// req.flash("success", "mail sent successfully");
						}
					});


				} else {
					var userData = { email: req.body.Email, password: pass, verification: verify, username: uname, Role: null };
					con.query("INSERT INTO verify SET ?", userData, (err, result) => {
						if (err) {
							console.log(err)
						} else {
							transporter.sendMail(mailOption, (error, info) => {
								if (error) {
									console.log(error)
								} else {

									let userdata = {
										email: `${req.body.Email}`,
									}
									// res.cookie("userinfo", userdata);
									res.send("Your Mail Send Successfully")
									res.json({ newUser: "newUser" })
								}
							});

							console.log('Data Successfully insert')
						}
					});
				}
			}
		});
	}
	bcrypt.genSalt(10, function (err, salt) {
		bcrypt.hash(req.body.Password, salt, function (err, hash) {
			if (err) {
				console.log(err);
			} else {
				Store(hash);
			}
		});
	});
})

app.get('/verification/', (req, res) => {
	// con.query("SELECT verify.verification FROM verify WHERE email = ?", req.cookies.userinfo.email, (err, result) => {
	// 	if (err) {
	// 		console.log(err);
	// 	} else {

	// 		activateAccount(result[0].verification);
	// 		/* var verify1 = req.query.verify;
	// 		var verify2 = result[0].verification; 
	// 		if(verify1 == verify2) {
	// 			activateAccount(result[0].verification);
	// 		}else{
	// 			res.send("<h1>verification fail</h1>")
	// 		} */
	// 	}
	// })
	// function activateAccount(verification) {
	// if (verification == req.query.verify) {
	con.query("UPDATE verify SET active = ? where verification = \'" + req.query.verify + "\'", "true", (err, result) => {
		if (err) {
			console.log(err);
		}
		else {
			let userdata = {
				email: `${req.body.Email}`,
				verify: "TRUE"
			}
			// res.cookie("userinfo", userdata);
			res.send('<h1>Account Verification Successfully</h1>');
		}
	})
	// }
	// else {
	// 	res.send("<h1>verification failed</h1>")
	// }
	// }
});

//app.set('view engine', 'ejs');
//app.set('views', __dirname + '/templates');
app.get("/dashboard", function (req, res) {
	res.sendFile(__dirname + '/templates/dashboard.html')
});

app.get("/forgot", function (req, res) {
	res.sendFile(__dirname + '/templates/forgot.html')
});

app.post('/forgot', (req, res) => {
	var email = req.body.Email;
	console.log(email);
	con.query('SELECT * FROM verify WHERE email = ?', email, (err, result) => {
		if (err) {
			console.log(err);
		} else {
			if (result[0] != null) {
				var success = false;
				var verify = result[0].verification;
				var mailOption = {
					from: 'neel2570@gmail.com', // sender this is your email here
					to: email, // receiver email2
					subject: "Reset Password",
					html: `<a href="http://localhost:8081/reset?verify=${verify}">CLICK HERE TO RESET PASSWORD</a>`
				}
				transporter.sendMail(mailOption, (error, info) => {
					if (error) {
						console.log(error)
					} else {
						console.log("Your Mail Send Successfully");
						success = true;
					}
				});
				res.json({ forgetPassResult: success })
			}
		}
	});

})

app.get("/reset", function (req, res) {
	res.sendFile(__dirname + '/templates/reset.html')
});

app.post("/reset", function (req, res, next) {
	var verificationCode = req.body.VerificationCode;
	var newpass = req.body.NewPassword;
	var success = true;
	bcrypt.genSalt(10, function (err, salt) {
		bcrypt.hash(newpass, salt, function (err, hash) {
			if (err) {
				console.log(err);
			} else {
				Update(hash);
			}
		});
	});
	function Update(hashpass) {
		con.query("UPDATE verify SET password = \'" + hashpass + "\' where verification = ?", verificationCode, (err, result) => {
			if (err) {
				console.log(err);
			}
			else {

			}
			//res.redirect("/");
		});



	}

});

app.get("/login", function (req, res) {
	console.log("login non-authd");
	res.sendFile(__dirname + "/templates/login.html");
});

app.post('/login', (req, res) => {
	var email = req.body.Email;
	var pass = req.body.Password;
	var userName = '';
	var userRole = '';
	//console.log(email);
	//console.log(pass);
	con.query('SELECT * FROM verify WHERE email = ?', email, (err, result) => {


		// if (err) {
		// 	console.log(err);
		// } else {


		if (result[0] != null) {
			var isValid = false;
			var hash = result[0].password;
			bcrypt.compare(pass, hash, function (err, res1) {
				isValid = res1;
				console.log(res1);
				if (isValid && result[0].active != 'true') {
					isValid = false;
				}

				else if (!isValid) {
					res.json({ msg: "ERROR" })
					console.log('fail');
				} else {
					userName = result[0].username;
					userRole = result[0].Role;
					LoginSuccess();
					console.log('success');
					//res.redirect('/views/dashboard.html');
				}
			});
			// }
		} else {
			console.log("Error");
			res.json({ msg1: "ERROR1" })
		}



	})
	function LoginSuccess() {
		//console.log(userName);
		let userdata = {
			email: `${req.body.Email}`,
			verify: "true",
			username: userName,
			role: userRole
		}
		// res.cookie("userinfo", userdata);

		res.json({ verify: "true", userinfo: JSON.stringify(userdata) });
	}
})

app.post("/api/token", function (req, res) {

	if (validateJWT(req.body.toString())) {
		res.status(200);
		res.end(generateToken({ "sub": currentUser, "salt": req.body.Password }));
	}

	res.status(401);
	res.end("Unauthorized");
});

// Auth required
app.get("/account", function (req, res) {
	console.log("sending account.html");
	//res.sendFile(__dirname + "/www/account.html",{},(error)=>{console.log(error)});
	var fs = require("fs");
	var file = fs.readFileSync(__dirname + "/templates/account.html");
	res.status(200);
	res.end(file);
	//return res.redirect("/account");
});

// Auth required
app.get("/settings", isValidCreds, function (req, res) {
	console.log("sending settings.html");
	//res.sendFile(__dirname + "/www/account.html",{},(error)=>{console.log(error)});
	var fs = require("fs");
	var file = fs.readFileSync(__dirname + "/templates/settings.html");
	res.status(200);
	res.end(file);
	//return res.redirect("/settings");
});


// app.get("/dashboard", isValidCreds, function (req, res) {
// 	console.log("Sending dashboard.html");
// 	var fs = require("fs");
// 	var file = fs.readFileSync(__dirname + "/templates/dashboard.html");
// 	res.status(200);
// 	res.end(file);
// });


// start the server
app.listen(port);
console.log("Server started at http://localhost:" + port);

function generateToken(payload) {
	var header = {
		"alg": "HS256",
		"typ": "JWT"
	};

	var wordarrayHeader = CryptoJS.enc.Utf8.parse(JSON.stringify(header));
	var wordarrayPayload = CryptoJS.enc.Utf8.parse(JSON.stringify(payload));
	var encodedHeader = CryptoJS.enc.Base64.stringify(wordarrayHeader);
	var encodedPayload = CryptoJS.enc.Base64.stringify(wordarrayPayload);

	var salt = payload.salt;
	var secret = secrets[payload.sub].secret;

	var signature = CryptoJS.enc.Base64.stringify(
		CryptoJS.enc.Utf8.parse(
			CryptoJS.HmacSHA256(encodedHeader + "." + encodedPayload, salt + secret).toString(CryptoJS.enc.Hex)
		)
	);

	return [encodedHeader, encodedPayload, signature].join('.');
}

function isValidCreds(req, res, next) {

	if (hasValidated(req)) {
		next();
	}
	res.status(401);
	res.end("Unauthorized");
}

function hasValidated(req) {
	var authz = req.headers["authorize"];
	if (typeof authz !== 'undefined') {
		var authzParts = authz.split(" ");
		if (authzParts[0] === "Bearer") {

			if (validateJWT(authzParts[1])) {
				console.log("Authorized");
				return true;
			}
		}
	}
	return false;
}

function validateJWT(jwtstring) {

	var jwtParts = jwtstring.split(".");
	var header = JSON.parse(Buffer.from(jwtParts[0], 'base64').toString());
	var payload = JSON.parse(Buffer.from(jwtParts[1], 'base64').toString());
	var signature = jwtParts[2];

	var encodedHeader = Buffer.from(JSON.stringify(header)).toString('base64');
	var encodedPayload = Buffer.from(JSON.stringify(payload)).toString('base64');

	var username = payload.sub;
	currentUser = username;

	var secret = secrets[username].secret;
	var salt = payload.salt;
	secrets[username].salt = salt;

	var veriSign = CryptoJS.enc.Base64.stringify(
		CryptoJS.enc.Utf8.parse(
			CryptoJS.HmacSHA256(encodedHeader + "." + encodedPayload, salt + secret).toString(CryptoJS.enc.Hex)
		)
	);
	veriSign = veriSign.replace('/', '_');
	veriSign = veriSign.replace('+', '-');

	return veriSign === signature;
}
